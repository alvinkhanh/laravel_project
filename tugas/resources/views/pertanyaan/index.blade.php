@extends('layout.master')
@section('content')
@section('title')
<h1>Selamat Datang</h1>
@endsection
<div class="card " id="asuransi">
        <!-- header -->
        <div class="section header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12" >
                        <div class="card-body" id="asuransi">
                        <h1 class="text-center">Daftar Asuransi</h1>
                            <!-- tambah -->
                            <br><br>
                            <form action="/pertanyaan/{{$value->id}}" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                    <a href="/pertanyaan/create" type="button" class="btn btn-primary">Tambah pertanyaan</a>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                            </div>
                            </form>
                            <br>
      <!-- close search -->
      <!-- table -->
                    <div class="table-responsive" >
                            <table class="table" cellpadding="10" border="1" cellspacing="0">
                                    <tr class="active">
                                    <th>No.</th>
                                    <th>Aksi</th>
                                    <th>Judul</th>
                                    <th>Isi</th>
                                </tr>
                                <?php $i = 1; ?>
                                <?php foreach ($pertanyaan as $row=>value) : ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td>
                                    <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                                    <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Ed</a>
                                    <td>{{value->judul}}</td>
                                    <td>{{value->isi}}</td>
                                    @scrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1 value="Delete">
                                    @empty
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                         </table>
                     </div>
                     </div>
                 </div>
             </div>
        </div>
  </div>
@endsection
